CXX=g++

CXXFLAGS+=-Wall -Wextra -pedantic -std=c++11
LDFLAGS+=-lpng -lrt -ltbb
SRC+=src/main.cc

OBJ=$(SRC:.cc=.o)

NAME=cpg

all: $(NAME)

$(NAME): $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

clean:
	$(RM) $(OBJ)
	$(RM) $(NAME)
