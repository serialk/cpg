#include <chrono>
#include <cstring>
#include <iostream>

#include <boost/gil/gil_all.hpp>
#include <boost/gil/extension/io/png_dynamic_io.hpp>
#include <tbb/tbb.h>

#include "som.hh"

void usage(const std::string& name)
{
    std::cout << "usage: " << name << " [-p] [-n N] <input.png> <output>";
    std::cout << std::endl;
}

boost::gil::rgb8_image_t kohonen(const boost::gil::rgb8_view_t& input,
        unsigned n_iter, bool parallel)
{
    std::vector<std::vector<double>> colors;
    for (unsigned i = 0; i < input.width(); i++)
        for (unsigned j = 0; j < input.height(); j++)
            colors.emplace_back(std::vector<double>{
                    static_cast<double>(input(i, j)[0]) / 255,
                    static_cast<double>(input(i, j)[1]) / 255,
                    static_cast<double>(input(i, j)[2]) / 255,
            });

    som s(input.width(), input.height(), n_iter, parallel);
    s.training(colors);

    boost::gil::rgb8_image_t out(input.width(), input.height());
    s.render_on(boost::gil::view(out));

    return out;
}

int main(int argc, char *argv[])
{
    tbb::task_scheduler_init init;

    std::string input_name = "";
    std::string output_name = "";
    unsigned n_iter = 1000;
    bool parallel = false;

    for (int i = 1; i < argc; ++i)
    {
        if (!std::strcmp(argv[i], "-n"))
        {
            if (i >= argc - 1 || !(n_iter = std::atoi(argv[i + 1])))
            {
                usage(argv[0]);
                return 1;
            }
            ++i;
        }
        else if (!std::strcmp(argv[i], "-h"))
        {
            usage(argv[0]);
            return 0;
        }
        else if (!std::strcmp(argv[i], "-p"))
            parallel = true;
        else if (input_name == "")
            input_name = argv[i];
        else if (output_name == "")
            output_name = argv[i];
        else
        {
            usage(argv[0]);
            return 0;
        }
    }
    if (input_name == "" || output_name == "")
    {
        usage(argv[0]);
        return 0;
    }

    boost::gil::rgb8_image_t input;
    try {
        boost::gil::png_read_and_convert_image(input_name, input);
    }
    catch (std::ios_base::failure) {
        std::cerr << "error: cannot load file " << input_name << ".";
        std::cerr << std::endl;
        return 1;
    }

    auto t_start = std::chrono::steady_clock::now();
    auto output = kohonen(boost::gil::view(input), n_iter, parallel);
    auto t_end = std::chrono::steady_clock::now();

    try {
        boost::gil::png_write_view(output_name,
                boost::gil::const_view(output));
    }
    catch (std::ios_base::failure) {
        std::cerr << "error: cannot write file " << output_name << ".";
        std::cerr << std::endl;
        return 1;
    }

    std::chrono::duration<double> elapsed = t_end - t_start;
    std::cout << "elapsed time:  " << elapsed.count() << std::endl;
    std::cout << "per iteration: " << elapsed.count() / n_iter << std::endl;
    return 0;
}
