#ifndef NODE_HH
# define NODE_HH

# include <functional>
# include <cmath>
# include <random>
# include <vector>

# include <boost/gil/gil_all.hpp>

const unsigned num_weights = 3;

class node
{
public:
    node(int x, int y, std::mt19937& gen,
         std::uniform_real_distribution<double>& distrib)
      : x_(x)
      , y_(y)
    {
        weights_.reserve(num_weights);
        for (unsigned i = 0; i < num_weights; ++i)
            weights_.emplace_back(distrib(gen));
    }

    double distance(const std::vector<double>& input) const
    {
        double d = 0;
        for (unsigned i = 0; i < weights_.size(); ++i)
            d += (input[i] - weights_[i]) * (input[i] - weights_[i]);
        return std::sqrt(d);
    }

    void update_weights(const std::vector<double>& input, double learn_rate,
                        double influence)
    {
        for (unsigned i = 0; i < weights_.size(); ++i)
            weights_[i] += learn_rate * influence * (input[i] - weights_[i]);
    }

    void render_on(const boost::gil::rgb8_view_t& img) const
    {
        boost::gil::rgb8_pixel_t pixel(
            static_cast<int>(weights_[0] * 255),
            static_cast<int>(weights_[1] * 255),
            static_cast<int>(weights_[2] * 255));
        img(x_, y_) = pixel;
    }

    int get_x() const { return x_; }
    int get_y() const { return y_; }

private:
    std::vector<double> weights_;
    int x_;
    int y_;
};

#endif /* !NODE_HH */
