#ifndef SOM_HH
# define SOM_HH

# include <cassert>
# include <cmath>
# include <vector>
# include <memory>

# include <boost/gil/gil_all.hpp>
# include <tbb/tbb.h>

# include "node.hh"

class som
{
public:
    som(unsigned width, unsigned height, unsigned n_iter, bool parallel)
      : learn_rate_(0.1)
      , curr_iter_(1)
      , max_iter_(n_iter)
      , parallel_(parallel)
      , gen_(std::random_device{}())
    {
        std::uniform_real_distribution<double> distrib(0.0, 1.0);

        std::function<void(int)> init_neuron = [&](int i) {
            neurons_.emplace_back(i / height, i % height, gen_, distrib);
        };

        for (unsigned i = 0; i < width * height; ++i)
            init_neuron(i);

        radius_ = (double) std::max(width, height) / 2;
        time_ = max_iter_ / std::log(radius_);
    }

    unsigned bmu(const std::vector<double>& v) const
    {
        unsigned win = -1;
        double lowest = std::numeric_limits<double>::max();
        for (unsigned i = 0; i < neurons_.size(); ++i)
        {
            double d = neurons_[i].distance(v);
            if (d < lowest)
            {
                lowest = d;
                win = i;
            }
        }
        return win;
    }

    unsigned parallel_bmu(const std::vector<double>& v) const
    {
        using wpair_t = std::pair<double, unsigned>;

        auto pair = tbb::parallel_reduce(
            tbb::blocked_range<unsigned>(0, neurons_.size()),
            wpair_t{std::numeric_limits<double>::max(), ~0},

            [&](const tbb::blocked_range<unsigned>& range, wpair_t init)
              -> wpair_t
            {
                for (unsigned i = range.begin(); i < range.end(); ++i)
                {
                    double d = neurons_[i].distance(v);
                    if (d < init.first)
                    {
                        init.first = d;
                        init.second = i;
                    }
                }
                return init;
            },

            [&](wpair_t x, wpair_t y) -> wpair_t {
                return (x.first < y.first) ? x : y;
            }
        );

        return pair.second;
    }

    void iteration(const std::vector<std::vector<double>>& input,
                   unsigned curr)
    {
        assert(input[0].size() == num_weights);
        unsigned winning = (parallel_ ? parallel_bmu(input[curr])
                                      : bmu(input[curr]));

        double neighbour_radius = radius_ *
            std::exp(-static_cast<double>(curr_iter_) / time_);
        double nbrs = neighbour_radius * neighbour_radius;

        std::function<void(int)> update_neuron = [&](int i) {
            double distx = neurons_[winning].get_x() - neurons_[i].get_x();
            double disty = neurons_[winning].get_y() - neurons_[i].get_y();
            distx *= distx;
            disty *= disty;

            double dist = distx + disty;
            if (dist < nbrs)
            {
                double infl = std::exp(-dist / (2 * nbrs));
                neurons_[i].update_weights(input[curr], learn_rate_, infl);
            }
        };

        if (!parallel_)
            for (unsigned i = 0; i < neurons_.size(); i++)
                update_neuron(i);
        else
            tbb::parallel_for(0, static_cast<int>(neurons_.size()), 1,
                              update_neuron);

        learn_rate_ = 0.1 * std::exp(-static_cast<double>(curr_iter_) /
                max_iter_);
        ++curr_iter_;
    }

    void training(const std::vector<std::vector<double>>& input)
    {
        std::uniform_int_distribution<int> distribution(0, input.size() - 1);
        auto dice = std::bind(distribution, gen_);

        while (curr_iter_ < max_iter_)
            iteration(input, dice());
    }

    void render_on(const boost::gil::rgb8_view_t& img)
    {
        for (const auto& n: neurons_)
            n.render_on(img);
    }

private:
    std::vector<node> neurons_;

    double radius_;
    double time_;
    double learn_rate_;

    unsigned curr_iter_;
    unsigned max_iter_;
    bool parallel_;

    std::mt19937 gen_;
};

#endif /* !SOM_HH */
